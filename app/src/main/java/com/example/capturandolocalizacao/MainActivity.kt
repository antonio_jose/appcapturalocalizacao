package com.example.capturandolocalizacao

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.lang.reflect.Array
import java.text.DecimalFormat
import java.util.ArrayList
import java.util.jar.Manifest

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    lateinit var mMap: GoogleMap

    lateinit var tv_valor_latitude : TextView
    lateinit var tv_valor_longitude : TextView

    var latitude = 0.0
    var longitude = 0.0


    lateinit var locactionManager: LocationManager
    var gpsAtivo: Boolean = true

    val permissoesRequeridas = arrayListOf(
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        locactionManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        gpsAtivo = locactionManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

        tv_valor_latitude = findViewById(R.id.txtValorLatitude)
        tv_valor_longitude = findViewById(R.id.txtValorLongitude)

        if(gpsAtivo){

            obterCoordenadas()

        }else{

            tv_valor_latitude.text = formataGeopoint( latitude )
            tv_valor_longitude.text = formataGeopoint( longitude )

            Toast.makeText(this, "Coordenadas não disponiveis . . .", Toast.LENGTH_SHORT).show()
        }

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap

        val madalena = LatLng(latitude, longitude)
        mMap.addMarker(MarkerOptions().position(madalena).title("Madalena-ce"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(madalena))
        mMap.setMaxZoomPreference(20.0F)
        mMap.setMinZoomPreference(2.0F)
        mMap.uiSettings.setZoomControlsEnabled(true)
        //mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

    }

    fun obterCoordenadas(){

      var permissoesAtivas = verificarPermissoes()

        if(permissoesAtivas){
            capturaUltimaLocalizacaoUsuario()
        }else{
            Toast.makeText(this, "Permissões não concedidas . . .", Toast.LENGTH_SHORT).show()
        }

    }


    fun verificarPermissoes(): Boolean{

        var permissoesNegadas : MutableList<Int> = ArrayList()
        var permissaoNegada : Int

        for(permissao in permissoesRequeridas){

            permissaoNegada = ContextCompat.checkSelfPermission(this, permissao)

            if( permissaoNegada != PackageManager.PERMISSION_GRANTED ){
                    permissoesNegadas.add(permissaoNegada)
            }

        }

        return  if(!permissoesNegadas.isEmpty()){

            ActivityCompat.requestPermissions(this, permissoesRequeridas.toTypedArray(), 100)
            return false

        }else{
            return true
        }



    }

    @SuppressLint("MissingPermission")
    fun capturaUltimaLocalizacaoUsuario(){

            var ultimaLocalizacao = locactionManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)

            latitude = ultimaLocalizacao!!.latitude
            longitude = ultimaLocalizacao!!.longitude

            tv_valor_latitude.text = formataGeopoint( latitude )
            tv_valor_longitude.text = formataGeopoint( longitude )

           Toast.makeText(this, "Coordenada obtidas com sucesso . . .", Toast.LENGTH_SHORT).show()

    }

    fun formataGeopoint(valor: Double) : String{

        var decimalFormat = DecimalFormat("#.####")
        return decimalFormat.format( valor )
    }


}